﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Piteste.Models
{
    public class AnoLetivo
    {
        public int AnoLetivoID { get; set; }
        
        public int Ativo { get; set; }        public int Ano { get; set; }

        public virtual ICollection<Classe> Classes { get; set; }
    }
}