﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Piteste.Models
{
    public class Pessoa
    {
        public int PessoaID { get; set; }
        public string PessoaEmail { get; set; }
        public string PessoaSenha { get; set; }
        public int PessoaTipo { get; set; }
    }
}