﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Piteste.Models
{
    public class Responsavel
    {
        public int ResponsavelID { get; set; }
        
        public string CpfResponsavel { get; set; }
        public string AtivoResponsavel { get; set; }
        public int TipoResponsavel { get; set; }

        public string NomeResponsavel { get; set; }
        public virtual ICollection<Aluno> Alunos { get; set; }
    }
}