﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Piteste.Models
{
    public class Aluno
    {
        public int AlunoID { get; set; }
        
        public string EnderecoAluno { get; set; }
        public int AtivoAluno { get; set; }
        public string NomeAluno { get; set; }
        public string CpfAluno { get; set; }
        public int TipoAluno { get; set; }


        public int ClasseID { get; set; }
        public int ResponsavelID { get; set; }        

        public virtual Classe Classe { get; set; }
        public virtual Responsavel Responsavel { get; set; }
    }
}