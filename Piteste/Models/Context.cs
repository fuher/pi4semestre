﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
namespace Piteste.Models
{
    public class Context : DbContext
    {
        public Context() : base("Piteste")
        {
            Configuration.ProxyCreationEnabled = false;
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<Context>());
        }
        public DbSet<AnoLetivo> Anos { get; set; }
        public DbSet<Responsavel> Responsaveis { get; set; }
        public DbSet<Aluno> Alunos{ get; set; }
        public DbSet<Professor> Professores { get; set; }
        public DbSet<Classe> Classes { get; set; }
        public DbSet<Pessoa> Pessoas { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}