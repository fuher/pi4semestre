﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Piteste.Models
{
    public class Classe
    {
        public int ClasseID { get; set; }
        
        public int Ativo { get; set; }
        public string Nome { get; set; }
        //fabricio

        public int AnoLetivoID { get; set; }
        public virtual AnoLetivo AnoLetivo { get; set; }



        public virtual ICollection<Aluno> Alunos { get; set; }
    }
}