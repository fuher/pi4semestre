﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Piteste.Models
{
    public class Professor
    {
        public int ProfessorID { get; set; }
        

        public string CpfProfessor { get; set; }
        public string EnderecoProfessor { get; set; }
        public int AtivoProfessor { get; set; }
        public int TipoProfessor { get; set; }
        public string EmailProfessor { get; set; }
        public string NomeProfessor { get; set; }
        
    }
}