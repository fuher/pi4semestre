﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Piteste.Models;
using Piteste.Repositories;

namespace Piteste.Controllers
{
    public class PublicoController : Controller
    {
        // GET: Publico
        public ActionResult Logar()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Logar(string email, string senha)
        {
            int tipo = Funcoes.AutenticarUsuario(email, senha);
            switch (tipo)
            {
                case 1:
                    return RedirectToAction("HomeAdm", "Pessoas");
                    
                case 2:
                    return RedirectToAction("HomeProfessores", "Professores");
                    
                case 3:
                    return RedirectToAction("HomeResponsavel", "Responsaveis");

                case 4:
                    return RedirectToAction("HomeAluno", "Alunos");

                default:
                    ViewBag.Error = "Nome de usuário e/ou senha inválida";
                    return View();
            }
            
                
            
            
        }        public ActionResult AcessoNegado()
        {
            using (Context c = new Context())
            {
                return View();
            }
        }
        public ActionResult Logoff()
        {
            Piteste.Repositories.Funcoes.Deslogar();
            return RedirectToAction("Logar", "Publico");
        }
    }
}