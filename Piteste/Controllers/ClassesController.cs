﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Piteste.Models;

namespace Piteste.Controllers
{
    public class ClassesController : Controller
    {
        private Context db = new Context();

        // GET: Classes
        public ActionResult Index()
        {
            var classes = db.Classes.Include(c => c.AnoLetivo);
            return View(classes.ToList());
        }

        // GET: Classes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Classe classe = db.Classes.Find(id);
            if (classe == null)
            {
                return HttpNotFound();
            }
            return View(classe);
        }

        // GET: Classes/Create
        public ActionResult Create()
        {
            ViewBag.AnoLetivoID = new SelectList(db.Anos, "AnoLetivoID", "AnoLetivoID");
            return View();
        }

        // POST: Classes/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ClasseID,Nome,Ativo,AnoLetivoID")] Classe classe)
        {
            if (ModelState.IsValid)
            {
                db.Classes.Add(classe);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AnoLetivoID = new SelectList(db.Anos, "AnoLetivoID", "AnoLetivoID", classe.AnoLetivoID);
            return View(classe);
        }

        // GET: Classes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Classe classe = db.Classes.Find(id);
            if (classe == null)
            {
                return HttpNotFound();
            }
            ViewBag.AnoLetivoID = new SelectList(db.Anos, "AnoLetivoID", "AnoLetivoID", classe.AnoLetivoID);
            return View(classe);
        }

        // POST: Classes/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ClasseID,Nome,Ativo,AnoLetivoID")] Classe classe)
        {
            if (ModelState.IsValid)
            {
                db.Entry(classe).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AnoLetivoID = new SelectList(db.Anos, "AnoLetivoID", "AnoLetivoID", classe.AnoLetivoID);
            return View(classe);
        }

        // GET: Classes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Classe classe = db.Classes.Find(id);
            if (classe == null)
            {
                return HttpNotFound();
            }
            return View(classe);
        }

        // POST: Classes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Classe classe = db.Classes.Find(id);
            db.Classes.Remove(classe);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
