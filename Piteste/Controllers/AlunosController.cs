﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Piteste.Models;

namespace Piteste.Controllers
{
    public class AlunosController : Controller
    {
        private Context db = new Context();

        // GET: Alunos
        public ActionResult Index()
        {
            var alunos = db.Alunos.Include(a => a.Classe).Include(a => a.Responsavel);
            return View(alunos.ToList());
        }
        public ActionResult HomeAluno()
        {
            return View();
        }
        // GET: Alunos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Aluno aluno = db.Alunos.Find(id);
            if (aluno == null)
            {
                return HttpNotFound();
            }
            return View(aluno);
        }

        // GET: Alunos/Create
        public ActionResult Create()
        {
            ViewBag.ClasseID = new SelectList(db.Classes, "ClasseID", "Nome");
            ViewBag.ResponsavelID = new SelectList(db.Responsaveis, "ResponsavelID", "CpfResponsavel");
            return View();
        }

        // POST: Alunos/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AlunoID,EnderecoAluno,AtivoAluno,NomeAluno,CpfAluno,ClasseID,ResponsavelID,TipoAluno")] Aluno aluno)
        {
            if (ModelState.IsValid)
            {
                db.Alunos.Add(aluno);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClasseID = new SelectList(db.Classes, "ClasseID", "Nome", aluno.ClasseID);
            ViewBag.ResponsavelID = new SelectList(db.Responsaveis, "ResponsavelID", "CpfResponsavel", aluno.ResponsavelID);
            return View(aluno);
        }

        // GET: Alunos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Aluno aluno = db.Alunos.Find(id);
            if (aluno == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClasseID = new SelectList(db.Classes, "ClasseID", "Nome", aluno.ClasseID);
            ViewBag.ResponsavelID = new SelectList(db.Responsaveis, "ResponsavelID", "CpfResponsavel", aluno.ResponsavelID);
            return View(aluno);
        }

        // POST: Alunos/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AlunoID,EnderecoAluno,AtivoAluno,NomeAluno,CpfAluno,ClasseID,ResponsavelID,TipoAluno")] Aluno aluno)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aluno).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClasseID = new SelectList(db.Classes, "ClasseID", "Nome", aluno.ClasseID);
            ViewBag.ResponsavelID = new SelectList(db.Responsaveis, "ResponsavelID", "CpfResponsavel", aluno.ResponsavelID);
            return View(aluno);
        }

        // GET: Alunos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Aluno aluno = db.Alunos.Find(id);
            if (aluno == null)
            {
                return HttpNotFound();
            }
            return View(aluno);
        }

        // POST: Alunos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Aluno aluno = db.Alunos.Find(id);
            db.Alunos.Remove(aluno);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
