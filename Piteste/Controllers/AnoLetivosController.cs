﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Piteste.Models;

namespace Piteste.Controllers
{
    public class AnoLetivosController : Controller
    {
        private Context db = new Context();

        // GET: AnoLetivos
        public ActionResult Index()
        {
            return View(db.Anos.ToList());
        }

        // GET: AnoLetivos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnoLetivo anoLetivo = db.Anos.Find(id);
            if (anoLetivo == null)
            {
                return HttpNotFound();
            }
            return View(anoLetivo);
        }

        // GET: AnoLetivos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AnoLetivos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AnoLetivoID,Ano,Ativo")] AnoLetivo anoLetivo)
        {
            if (ModelState.IsValid)
            {
                db.Anos.Add(anoLetivo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(anoLetivo);
        }

        // GET: AnoLetivos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnoLetivo anoLetivo = db.Anos.Find(id);
            if (anoLetivo == null)
            {
                return HttpNotFound();
            }
            return View(anoLetivo);
        }

        // POST: AnoLetivos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AnoLetivoID,Ano,Ativo")] AnoLetivo anoLetivo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(anoLetivo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(anoLetivo);
        }

        // GET: AnoLetivos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnoLetivo anoLetivo = db.Anos.Find(id);
            if (anoLetivo == null)
            {
                return HttpNotFound();
            }
            return View(anoLetivo);
        }

        // POST: AnoLetivos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AnoLetivo anoLetivo = db.Anos.Find(id);
            db.Anos.Remove(anoLetivo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
