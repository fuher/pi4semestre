namespace Piteste.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pessoa : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pessoa",
                c => new
                    {
                        PessoaID = c.Int(nullable: false, identity: true),
                        PessoaEmail = c.String(unicode: false),
                        PessoaSenha = c.String(unicode: false),
                        PessoaTipo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PessoaID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Pessoa");
        }
    }
}
