namespace Piteste.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class astfisahdg : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Responsavel", "TipoResponsavel", c => c.Int(nullable: false));
            DropColumn("dbo.Responsavel", "TipoAluno");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Responsavel", "TipoAluno", c => c.Int(nullable: false));
            DropColumn("dbo.Responsavel", "TipoResponsavel");
        }
    }
}
