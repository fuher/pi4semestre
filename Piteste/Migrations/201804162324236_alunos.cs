namespace Piteste.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class alunos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Aluno", "ClasseID", c => c.Int(nullable: false));
            AddColumn("dbo.Aluno", "ResponsavelID", c => c.Int(nullable: false));
            CreateIndex("dbo.Aluno", "ClasseID");
            CreateIndex("dbo.Aluno", "ResponsavelID");
            AddForeignKey("dbo.Aluno", "ClasseID", "dbo.Classe", "ClasseID", cascadeDelete: true);
            AddForeignKey("dbo.Aluno", "ResponsavelID", "dbo.Responsavel", "ResponsavelID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Aluno", "ResponsavelID", "dbo.Responsavel");
            DropForeignKey("dbo.Aluno", "ClasseID", "dbo.Classe");
            DropIndex("dbo.Aluno", new[] { "ResponsavelID" });
            DropIndex("dbo.Aluno", new[] { "ClasseID" });
            DropColumn("dbo.Aluno", "ResponsavelID");
            DropColumn("dbo.Aluno", "ClasseID");
        }
    }
}
