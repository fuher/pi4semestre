namespace Piteste.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class att3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Aluno", "EnderecoAluno", c => c.String(unicode: false));
            AddColumn("dbo.Aluno", "AtivoAluno", c => c.Int(nullable: false));
            AddColumn("dbo.Aluno", "NomeAluno", c => c.String(unicode: false));
            AddColumn("dbo.Aluno", "CpfAluno", c => c.String(unicode: false));
            AddColumn("dbo.Responsavel", "CpfResponsavel", c => c.String(unicode: false));
            AddColumn("dbo.Responsavel", "AtivoResponsavel", c => c.String(unicode: false));
            AddColumn("dbo.Responsavel", "NomeResponsavel", c => c.String(unicode: false));
            AddColumn("dbo.Professor", "CpfProfessor", c => c.String(unicode: false));
            AddColumn("dbo.Professor", "EnderecoProfessor", c => c.String(unicode: false));
            AddColumn("dbo.Professor", "AtivoProfessor", c => c.Int(nullable: false));
            AddColumn("dbo.Professor", "NomeProfessor", c => c.String(unicode: false));
            DropColumn("dbo.Aluno", "Endereco");
            DropColumn("dbo.Aluno", "Ativo");
            DropColumn("dbo.Aluno", "Nome");
            DropColumn("dbo.Aluno", "Cpf");
            DropColumn("dbo.Responsavel", "Cpf");
            DropColumn("dbo.Responsavel", "Ativo");
            DropColumn("dbo.Responsavel", "Nome");
            DropColumn("dbo.Professor", "Cpf");
            DropColumn("dbo.Professor", "Endereco");
            DropColumn("dbo.Professor", "Ativo");
            DropColumn("dbo.Professor", "Nome");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Professor", "Nome", c => c.String(unicode: false));
            AddColumn("dbo.Professor", "Ativo", c => c.Int(nullable: false));
            AddColumn("dbo.Professor", "Endereco", c => c.String(unicode: false));
            AddColumn("dbo.Professor", "Cpf", c => c.String(unicode: false));
            AddColumn("dbo.Responsavel", "Nome", c => c.String(unicode: false));
            AddColumn("dbo.Responsavel", "Ativo", c => c.String(unicode: false));
            AddColumn("dbo.Responsavel", "Cpf", c => c.String(unicode: false));
            AddColumn("dbo.Aluno", "Cpf", c => c.String(unicode: false));
            AddColumn("dbo.Aluno", "Nome", c => c.String(unicode: false));
            AddColumn("dbo.Aluno", "Ativo", c => c.Int(nullable: false));
            AddColumn("dbo.Aluno", "Endereco", c => c.String(unicode: false));
            DropColumn("dbo.Professor", "NomeProfessor");
            DropColumn("dbo.Professor", "AtivoProfessor");
            DropColumn("dbo.Professor", "EnderecoProfessor");
            DropColumn("dbo.Professor", "CpfProfessor");
            DropColumn("dbo.Responsavel", "NomeResponsavel");
            DropColumn("dbo.Responsavel", "AtivoResponsavel");
            DropColumn("dbo.Responsavel", "CpfResponsavel");
            DropColumn("dbo.Aluno", "CpfAluno");
            DropColumn("dbo.Aluno", "NomeAluno");
            DropColumn("dbo.Aluno", "AtivoAluno");
            DropColumn("dbo.Aluno", "EnderecoAluno");
        }
    }
}
