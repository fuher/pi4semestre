namespace Piteste.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class prof : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Professor",
                c => new
                    {
                        ProfessorID = c.Int(nullable: false, identity: true),
                        Nome = c.String(unicode: false),
                        Cpf = c.String(unicode: false),
                        Endereco = c.String(unicode: false),
                        Ativo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProfessorID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Professor");
        }
    }
}
