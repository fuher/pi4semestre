namespace Piteste.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Piteste.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Piteste.Models.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Piteste.Models.Context context)
        {
            IList<AnoLetivo> anos = new List<AnoLetivo>();
           //anos.Add(new AnoLetivo() { Ano = 2016, Ativo = 1 });
           // anos.Add(new AnoLetivo() { Ano = 2017, Ativo = 1 });
           // anos.Add(new AnoLetivo() { Ano = 2018, Ativo = 0 });
            foreach (AnoLetivo anoletivo in anos)
            {
                context.Anos.AddOrUpdate(x => x.AnoLetivoID, anoletivo);
            }
        }
    }
}
