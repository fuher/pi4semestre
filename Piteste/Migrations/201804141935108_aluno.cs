namespace Piteste.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class aluno : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Aluno",
                c => new
                    {
                        AlunoID = c.Int(nullable: false, identity: true),
                        Nome = c.String(unicode: false),
                        Endereco = c.String(unicode: false),
                        Ativo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AlunoID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Aluno");
        }
    }
}
