namespace Piteste.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ano : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnoLetivo",
                c => new
                    {
                        AnoLetivoID = c.Int(nullable: false, identity: true),
                        Ano = c.Int(nullable: false),
                        Ativo = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.AnoLetivoID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AnoLetivo");
        }
    }
}
