// <auto-generated />
namespace Piteste.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class att276 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(att276));
        
        string IMigrationMetadata.Id
        {
            get { return "201804182342161_att276"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
