namespace Piteste.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class classe : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Classe",
                c => new
                    {
                        ClasseID = c.Int(nullable: false, identity: true),
                        Nome = c.String(unicode: false),
                        Ativo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ClasseID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Classe");
        }
    }
}
