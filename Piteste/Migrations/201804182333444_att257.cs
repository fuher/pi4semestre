namespace Piteste.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class att257 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Aluno", "TipoAluno", c => c.Int(nullable: false));
            AddColumn("dbo.Responsavel", "TipoAluno", c => c.Int(nullable: false));
            AddColumn("dbo.Professor", "TipoProfessor", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Professor", "TipoProfessor");
            DropColumn("dbo.Responsavel", "TipoAluno");
            DropColumn("dbo.Aluno", "TipoAluno");
        }
    }
}
