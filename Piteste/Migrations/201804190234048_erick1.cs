namespace Piteste.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class erick1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Professor", "EmailProfessor", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Professor", "EmailProfessor");
        }
    }
}
