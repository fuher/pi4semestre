namespace Piteste.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class anoletivo2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AnoLetivo", "Ativo", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AnoLetivo", "Ativo", c => c.String(unicode: false));
        }
    }
}
