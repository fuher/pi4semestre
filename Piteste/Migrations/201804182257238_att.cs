namespace Piteste.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class att : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Aluno", "Cpf", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Aluno", "Cpf");
        }
    }
}
