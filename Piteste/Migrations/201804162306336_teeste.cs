namespace Piteste.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class teeste : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Classe", "AnoLetivoID", c => c.Int(nullable: false));
            CreateIndex("dbo.Classe", "AnoLetivoID");
            AddForeignKey("dbo.Classe", "AnoLetivoID", "dbo.AnoLetivo", "AnoLetivoID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Classe", "AnoLetivoID", "dbo.AnoLetivo");
            DropIndex("dbo.Classe", new[] { "AnoLetivoID" });
            DropColumn("dbo.Classe", "AnoLetivoID");
        }
    }
}
