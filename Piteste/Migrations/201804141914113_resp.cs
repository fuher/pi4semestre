namespace Piteste.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class resp : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Responsavel",
                c => new
                    {
                        ResponsavelID = c.Int(nullable: false, identity: true),
                        Nome = c.String(unicode: false),
                        Cpf = c.String(unicode: false),
                        Ativo = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.ResponsavelID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Responsavel");
        }
    }
}
