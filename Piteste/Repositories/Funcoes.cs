﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Piteste.Models;

namespace Piteste.Repositories
{
    public class Funcoes
    {
        public static int AutenticarUsuario(string login, string senha)
        {
            int tipo = 0;
            Context _db = new Context();
            var query = (from u in _db.Pessoas
                         where u.PessoaEmail == login &&
                         u.PessoaSenha == senha
                         select u).SingleOrDefault();
            if (query == null)
            {
                return 0;
            }
            FormsAuthentication.SetAuthCookie(query.PessoaEmail, false);
            //HttpContext.Current.Response.Cookies["Usuario"].Value = query.Email;
            //HttpContext.Current.Response.Cookies["Usuario"].Expires = DateTime.Now.AddDays(10);
            HttpContext.Current.Session["Usuario"] = query.PessoaEmail;
            return query.PessoaTipo;
        }
        public static Pessoa GetUsuario()
        {
            string _login = HttpContext.Current.User.Identity.Name;
            //if (HttpContext.Current.Request.Cookies.Count > 0 ||
            //HttpContext.Current.Request.Cookies["Usuario"] != null)
            if (HttpContext.Current.Session.Count > 0 ||HttpContext.Current.Session["Usuario"] != null)
            {
                _login = HttpContext.Current.Session["Usuario"].ToString();
                //_login = HttpContext.Current.Request.Cookies["Usuario"].Value.ToStri
                if (_login == "")
                {
                    return null;
                }
                else
                {
                    Context _db = new Context();
                    Pessoa pessoa = (from u in _db.Pessoas
                                       where u.PessoaEmail == _login
                                       select u).SingleOrDefault();
                    return pessoa;
                }
            }
            else
            {
                return null;
            }
        }
        public static Pessoa GetUsuario(string _login)
        {
            if (_login == "")
            {
                return null;
            }
            else
            {
                Context _db = new Context();
                Pessoa pessoa = (from u in _db.Pessoas
                                   where u.PessoaEmail == _login
                                   select u).SingleOrDefault();
                return pessoa;
            }
        }
        public static void Deslogar()
        {
            HttpContext.Current.Session["Usuario"] = "";
            //HttpContext.Current.Response.Cookies["Usuario"].Value = "";
            FormsAuthentication.SignOut();
        }
    }
}